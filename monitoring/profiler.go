// +build continuous_profiler_stackdriver

package monitoring

import (
	"net/url"
	"os"

	"cloud.google.com/go/profiler"
	"gitlab.com/gitlab-org/labkit/log"
)

const profilerEnvKey = "GITLAB_CONTINUOUS_PROFILING"

// Exposing to tests
var profStart = profiler.Start

func initProfiler() {
	driverParams, exists := os.LookupEnv(profilerEnvKey)
	if !exists {
		return
	}

	u, err := url.Parse(driverParams)
	if err != nil {
		log.WithError(err).Warn("unable to parse env var content")
		return
	}

	query := u.Query()
	driverName := u.Path

	if driverName != "stackdriver" {
		log.WithField("driver", driverName).Warn("unkown driver")
		return
	}

	config, err := initStackdriverProfiler(query)
	if err != nil {
		log.WithError(err).Warnf("unable to initialize %v profiler", driverName)
		return
	}

	log.WithFields(log.Fields{
		"driver":         driverName,
		"service":        config.Service,
		"serviceVersion": config.ServiceVersion,
		"projectId":      config.ProjectID,
	}).Info("profiler enabled")
}

func initStackdriverProfiler(query url.Values) (profiler.Config, error) {
	service := query.Get("service")
	serviceVersion := query.Get("service_version")
	projectID := query.Get("project_id")

	config := profiler.Config{
		// Falls to GAE_SERVICE env var if not given.
		Service: service,
		// Falls to GAE_VERSION env var if not given.
		ServiceVersion: serviceVersion,
		// Falls to GOOGLE_CLOUD_PROJECT env var or VM metadata if not given.
		ProjectID: projectID,
	}

	if err := profStart(config); err != nil {
		return config, err
	}

	return config, nil
}
