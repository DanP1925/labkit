// +build continuous_profiler_stackdriver

package monitoring

import (
	"bytes"
	"fmt"
	"net/url"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gitlab-org/labkit/log"

	"cloud.google.com/go/profiler"
	"github.com/stretchr/testify/require"
	"google.golang.org/api/option"
)

func TestInitStackdriverProfiler(t *testing.T) {
	tests := []struct {
		name                   string
		params                 string
		expectedService        string
		expectedServiceVersion string
		expectedProjectID      string
		expectedErr            error
	}{
		{
			name:                   "complete params",
			params:                 "stackdriver?service=gitaly&service_version=1.0.1&project_id=test-123",
			expectedService:        "gitaly",
			expectedServiceVersion: "1.0.1",
			expectedProjectID:      "test-123",
		},
		{
			name:                   "no service provided",
			params:                 "stackdriver?service_version=1.0.1&project_id=test-123",
			expectedService:        "",
			expectedServiceVersion: "1.0.1",
			expectedProjectID:      "test-123",
		},
		{
			name:                   "no service version provided",
			params:                 "stackdriver?service=gitaly&project_id=test-123",
			expectedService:        "gitaly",
			expectedServiceVersion: "",
			expectedProjectID:      "test-123",
		},
		{
			name:                   "no project_id provided",
			params:                 "stackdriver?service=gitaly&service_version=1.0.1",
			expectedService:        "gitaly",
			expectedServiceVersion: "1.0.1",
			expectedProjectID:      "",
		},
		{
			name:        "profiler fails to start",
			params:      "stackdriver",
			expectedErr: fmt.Errorf("service name must be configured"),
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			clearGcpEnvVars(t)

			preStubStart := profStart
			defer func() { profStart = preStubStart }()

			u, err := url.Parse(test.params)
			require.NoError(t, err)

			if test.expectedErr != nil {
				_, err := initStackdriverProfiler(u.Query())

				require.Error(t, err)
				assert.Equal(t, test.expectedErr, err)
			} else {
				stubStackdriverProfiler(nil)
				config, err := initStackdriverProfiler(u.Query())

				require.NoError(t, err)
				assert.Equal(t, test.expectedService, config.Service, "Service should match")
				assert.Equal(t, test.expectedServiceVersion, config.ServiceVersion, "ServiceVersion should match")
				assert.Equal(t, test.expectedProjectID, config.ProjectID, "ProjectID version should match")
			}
		})
	}
}

func TestInitProfiler(t *testing.T) {
	tests := []struct {
		name        string
		params      string
		expectedLog string
		profErr     error
	}{
		{
			name:        "complete params",
			params:      "stackdriver?service=gitaly&service_version=1.0.1&project_id=test-123",
			expectedLog: `level=info msg="profiler enabled" driver=stackdriver projectId=test-123 service=gitaly serviceVersion=1.0.1`,
		},
		{
			name:        "unknown driver",
			params:      "true?service=gitaly",
			expectedLog: `level=warning msg="unkown driver" driver=true`,
		},
		{
			name:        "wrong env content format",
			params:      ":foo?service=gitaly",
			expectedLog: `level=warning msg="unable to parse env var content"`,
		},
		{
			name:        "fails to start profiler",
			params:      "stackdriver?service=gitaly&service_version=1.0.1&project_id=test-123",
			expectedLog: `level=warning msg="unable to initialize stackdriver profiler" error="fail to start"`,
			profErr:     fmt.Errorf("fail to start"),
		},
	}
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			clearGcpEnvVars(t)

			preStubStart := profStart
			defer func() { profStart = preStubStart }()
			stubStackdriverProfiler(test.profErr)

			err := os.Setenv(profilerEnvKey, test.params)
			require.NoError(t, err)

			logOutput := captureLogOutput(t, initProfiler)

			require.Regexp(t, test.expectedLog, logOutput)
		})
	}
}

func TestInitProfilerWithNoProfilerEnvKeySet(t *testing.T) {
	t.Run("no GITLAB_CONTINUOUS_PROFILING env var set", func(t *testing.T) {
		clearGcpEnvVars(t)

		logOutput := captureLogOutput(t, initProfiler)

		require.Empty(t, logOutput)
	})
}

func stubStackdriverProfiler(err error) {
	profStart = func(cfg profiler.Config, options ...option.ClientOption) error {
		return err
	}
}

func captureLogOutput(t *testing.T, init func()) string {
	buf := &bytes.Buffer{}
	closer, err := log.Initialize(log.WithWriter(buf))
	require.NoError(t, err)
	defer closer.Close()

	init()

	return buf.String()
}

func clearGcpEnvVars(t *testing.T) {
	for _, env := range []string{profilerEnvKey, "GAE_SERVICE", "GAE_VERSION", "GOOGLE_CLOUD_PROJECT"} {
		err := os.Unsetenv(env)

		require.NoError(t, err)
	}
}
