package errortracking

import (
	"testing"

	"github.com/getsentry/raven-go"
	"github.com/stretchr/testify/require"
)

func TestWithField(t *testing.T) {
	var interfaces []raven.Interface
	extra := raven.Extra{}
	domain := "http://example.com"

	WithField("domain", domain)(interfaces, extra)

	require.True(t, extra["domain"] == domain)
}
